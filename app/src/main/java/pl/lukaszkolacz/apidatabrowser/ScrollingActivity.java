package pl.lukaszkolacz.apidatabrowser;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import pl.lukaszkolacz.apidatabrowser.adapters.UsersAdapter;

public class ScrollingActivity extends AppCompatActivity {
    private static final String TAG = "ScrollingActivity";
    RecyclerView recyclerView;
    UsersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.users_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new UsersAdapter(this);
        recyclerView.setAdapter(adapter);


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.getUsers();
            }
        });

        final LinearLayout searchLay = (LinearLayout) findViewById(R.id.search_lay);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0 || dy < 0 || !fab.isShown())
//                    searchLay.setVisibility(View.GONE);

                if (fab.isShown()) {
                    searchLay.setVisibility(View.VISIBLE);
                }else {
                    searchLay.setVisibility(View.GONE);
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                if (fab.isShown()) {
                    searchLay.setVisibility(View.VISIBLE);
                }else {
                    searchLay.setVisibility(View.GONE);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        SearchView searchView = (SearchView) findViewById(R.id.user_search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.searchByName(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.searchByName(newText);
                return true;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.cancelSearch();
                return true;
            }
        });
    }



}
