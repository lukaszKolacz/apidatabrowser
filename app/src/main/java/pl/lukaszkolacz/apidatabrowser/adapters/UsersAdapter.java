package pl.lukaszkolacz.apidatabrowser.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import pl.lukaszkolacz.apidatabrowser.ApiDataApp;
import pl.lukaszkolacz.apidatabrowser.R;
import pl.lukaszkolacz.apidatabrowser.UserDetailActivity;
import pl.lukaszkolacz.apidatabrowser.model.ApiUsers;
import pl.lukaszkolacz.apidatabrowser.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lukasz Kolacz on 25.08.2017.
 */

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<User> data = new ArrayList<>();
    private ArrayList<User> saveData = new ArrayList<>();

    public UsersAdapter(Context context) {
        this.mContext = context;
        getUsers();
    }

    public void getUsers() {
        ApiDataApp.usersApi.getUsers(30).enqueue(new Callback<ApiUsers>() {
            @Override
            public void onResponse(Call<ApiUsers> call, Response<ApiUsers> response) {
                if (response.isSuccessful()) {
                    data.clear();
                    for (User user : response.body().results) {
                        data.add(user);
                        saveData.add(user);
                    }
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(mContext, "Zjebalo sie", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApiUsers> call, Throwable t) {
                Toast.makeText(mContext, "No internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext).inflate(R.layout.user_item_lay, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final User user = data.get(position);
        holder.uName.setText(user.name.first + " " + user.name.last);
        holder.uEmail.setText(user.email);
        Picasso.with(mContext).load(user.picture.medium).into(holder.uImage);
        holder.uRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, UserDetailActivity.class);
                intent.putExtra("userData", new Gson().toJson(user));
                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(((AppCompatActivity) mContext),holder.uImage , "detail_transition");
                ((AppCompatActivity) mContext).startActivityForResult(intent,303,optionsCompat.toBundle());
            }
        });

        holder.ufavorite.setChecked(user.favorite);
        holder.ufavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                data.get(holder.getAdapterPosition()).favorite = b;
            }
        });
    }

    public void searchByName(String query) {
        data = new ArrayList<>();
        for (User user : saveData) {
            if (user.name.first.startsWith(query)) {
                data.add(user);
            }
        }
        notifyDataSetChanged();
    }

    public void cancelSearch() {
        data = new ArrayList<>(saveData);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        View uRoot;
        ImageView uImage;
        TextView uName;
        TextView uEmail;
        CheckBox ufavorite;

        public ViewHolder(View itemView) {
            super(itemView);
            uImage = itemView.findViewById(R.id.user_image);
            uName = itemView.findViewById(R.id.user_name);
            uEmail = itemView.findViewById(R.id.user_email);
            uRoot = itemView.findViewById(R.id.user_root);
            ufavorite = itemView.findViewById(R.id.user_favorite);
        }
    }
}
