package pl.lukaszkolacz.apidatabrowser;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import pl.lukaszkolacz.apidatabrowser.model.User;

public class UserDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        User user = new Gson().fromJson(getIntent().getExtras().getString("userData"), User.class);

        ImageView imageView = (ImageView) findViewById(R.id.detail_image);
        Picasso.with(this).load(user.picture.large).into(imageView);

        getSupportActionBar().setTitle(user.name.title + ". " + user.name.first + " " + user.name.last);

        ((TextView) findViewById(R.id.detail_location_street)).setText(user.location.street);
        ((TextView) findViewById(R.id.detail_location_city)).setText(user.location.city);
        ((TextView) findViewById(R.id.detail_location_state)).setText(user.location.state);
        ((TextView) findViewById(R.id.detail_location_postcode)).setText(user.location.postcode);

        ((TextView) findViewById(R.id.detail_contact_email)).setText(user.email);
        ((TextView) findViewById(R.id.detail_contact_phone)).setText(user.phone);
        ((TextView) findViewById(R.id.detail_contact_cell)).setText(user.cell);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
