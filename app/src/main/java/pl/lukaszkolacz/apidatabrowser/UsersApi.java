package pl.lukaszkolacz.apidatabrowser;

import pl.lukaszkolacz.apidatabrowser.model.ApiUsers;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Lukasz Kolacz on 25.08.2017.
 */

public interface UsersApi {
    @GET("api") Call<ApiUsers> getUsers(@Query("results") int count);
}
