
package pl.lukaszkolacz.apidatabrowser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Info {

    @SerializedName("seed")
    @Expose
    public String seed;
    @SerializedName("results")
    @Expose
    public Long results;
    @SerializedName("page")
    @Expose
    public Long page;
    @SerializedName("version")
    @Expose
    public String version;

}
