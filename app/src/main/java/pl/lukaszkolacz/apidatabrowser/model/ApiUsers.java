
package pl.lukaszkolacz.apidatabrowser.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiUsers {

    @SerializedName("results")
    @Expose
    public List<User> results = null;
    @SerializedName("info")
    @Expose
    public Info info;

}
