package pl.lukaszkolacz.apidatabrowser;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lukasz Kolacz on 25.08.2017.
 */

public class ApiDataApp extends Application {
    private static final String BASE_URL = "https://randomuser.me/";

    Retrofit retrofit;
    public static UsersApi usersApi;

    @Override
    public void onCreate() {
        super.onCreate();
        retrofit = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        usersApi = retrofit.create(UsersApi.class);
    }
}
